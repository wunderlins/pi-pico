# uPython with pi pico

## Getting started

- [Pico-Go VSCode Extension](http://pico-go.net/docs/start/quick/)
- [Getting started with Thonny](https://projects.raspberrypi.org/en/projects/getting-started-with-the-pico/2)
- [low level interaction with `picotool`](https://github.com/raspberrypi/picotool)
- [Make Pi Pico Example Code](https://github.com/CytronTechnologies/MAKER-PI-PICO) 

![Maker Pi Pico](../documentation/MakerPiPico-PinRef-Front-rev1.2-800-lres.png)

[Schemantic r1.2.0](../documentation/maker-pi-pico/MAKER-PI-PICO%20v1.2.0%20Schematic.pdf)

## prepare vscode

- install extension `Pico-Go`
- `Shift+Ctrl P` » "Pico-Go > Configure Project"

## Prepare Hardware

1. Download [pi micropython UF2](https://micropython.org/download/rp2-pico/rp2-pico-latest.uf2)
2. Flash Pico
   1. Hlod down BOOTSEL Button
   2. Connect USB port while still holding BOOTSEL
   3. New drive should appear
   4. Copy the Firmware (.uf2 File) onto drive
   5. Pico should reboot
   6. Done
