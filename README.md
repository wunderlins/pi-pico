# pi-pico

Experiments for the Rasperri Pi Pico. To get this repo setup, do:

```bash
git clone git@gitlab.com:wunderlins/pi-pico.git pi-pico
cd pi-pico 
git submodule update --init --recursive pico-c-examples/ pico-micropython-examples/ pico-project-generator/
# the pico-sdk should be installed globally somewhere, if not do:
#
# git submodule update --init --recursive pico-sdk
# this will take some time, pico-sdk has many sub modules
# 
# checking out all submodules and libraries will also 
# make vscode explode because it starts indexing all code files
#
# make sure picosdk can be found
export PICO_SDK_PATH="`pwd`/pico-sdk"
```

Updating submodules

```bash
cd pi-pico 
git submodule update --recursive --remote
```

## Resources

![Pinout](documentation/pic-pinout.png)

HackadayU - Raspberry Pi Pico and RP2040 - [The Deep Dive](https://hackaday.io/course/178733-raspberry-pi-pico-and-rp2040-the-deep-dive) / [Course Instructions (PDF)](documentation/Pi%20Pico%20Class%20Resources.pdf)
*ARM Assembly, PIO, and System Architecture - An intense Microcontroller Internals Course*


## Official Documentation for RP2040 / pi pico

- [Pi Pico Pinout Diagram (A4)](documentation/Pico-R3-A4-Pinout.pdf)
- [Raspberry Pi Pico Datasheet](documentation/pico-datasheet.pdf)
- [Getting started with Pi Pico (C/C++)](documentation/getting-started-with-pico.pdf)
- [Raspberry Pi Pico C/C++ SDK](documentation/raspberry-pi-pico-c-sdk.pdf)
- [Raspberry Pi Pico Python SDK](documentation/raspberry-pi-pico-python-sdk.pdf)
- [RP2040 Datasheet](documentation/rp2040-datasheet.pdf)
- [Hardware Design with RP2040](documentation/hardware-design-with-rp2040.pdf)

### C SDK

- [Pico C-SDK](https://raspberrypi.github.io/pico-sdk-doxygen/index.html)
- [Pico C-SDK Examples](https://github.com/raspberrypi/pico-examples)
- [Pico Project Generator (for cmake)](https://github.com/raspberrypi/pico-project-generator)

### Micropython resources

- [pi micropython UF2](https://micropython.org/download/rp2-pico/rp2-pico-latest.uf2)
- [Pico micropython overview](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html)
- [Pico uPython examples](https://github.com/raspberrypi/pico-micropython-examples)
- [Getting started with Raspberry Pi Pico](https://projects.raspberrypi.org/en/projects/getting-started-with-the-pico)
- [Quick reference](https://docs.micropython.org/en/latest/rp2/quickref.html)
- [RP2 Library](https://docs.micropython.org/en/latest/library/rp2.html)


### ASM

- [Thumb 2 Quck Reference *Cortex-M*](documentation/QRC0006_UAL16.pdf)
- [ARMv6-M Architecture Reference Manual](documentation/DDI0419C_arm_architecture_v6m_reference_manual.pdf)
- [Procedure Call Standard for the Arm Architecture *Release 2020Q2*](documentation/IHI0042J_2020Q2_aapcs32.pdf)
- [HackadayU - The Deep Dive](documentation/Pi%20Pico%20Class%20Resources.pdf)