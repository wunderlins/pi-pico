#include <stdint.h>

typedef struct divmod_results_t {
    int32_t quotient;
    int32_t reminder;
} divmod_results_t;

typedef struct divmod_resultu_t {
    uint32_t quotient;
    uint32_t reminder;
} divmod_resultu_t;

/**
 * @brief hardware integer (signed) divisor
 * 
 * This is an RP2040 hardware intege divisor. The result is placed into
 * The pointers passed in as arguments.
 * 
 * @param dividend 
 * @param divisor  
 */
extern divmod_results_t divmod_s32(int32_t dividend, int32_t divisor);

/**
 * @brief hardware integer (unsigned) divisor
 * 
 * This is an RP2040 hardware intege divisor. The result is placed into
 * The pointers passed in as arguments.
 * 
 * @param dividend 
 * @param divisor  
 */
extern divmod_resultu_t divmod_u32(uint32_t dividend, uint32_t divisor);