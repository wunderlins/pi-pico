# Generated Cmake Pico project file

cmake_minimum_required(VERSION 3.13)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# Initialise pico_sdk from installed location
# (note this can come from environment, CMake cache etc)
set(PICO_SDK_PATH "/opt/pico-sdk")

# Pull in Raspberry Pi Pico SDK (must be before project)
include(pico_sdk_import.cmake)

project(6-asm-integer-divier C CXX ASM)

# Initialise the Raspberry Pi Pico SDK
pico_sdk_init()

# Add executable. Default name is the project name, version 0.1

add_executable(6-asm-integer-divier 
    int-div.s
    6-asm-integer-divier.c
)

pico_set_program_name(6-asm-integer-divier "6-asm-integer-divier")
pico_set_program_version(6-asm-integer-divier "0.1")

pico_enable_stdio_uart(6-asm-integer-divier 1)
pico_enable_stdio_usb(6-asm-integer-divier 1)

# Add the standard library to the build
target_link_libraries(6-asm-integer-divier pico_stdlib)

pico_add_extra_outputs(6-asm-integer-divier)

