//.syntax unified
.equ SIO_BASE,                 0xd0000000

.equ SIO_DIV_UDIVIDEND_OFFSET, 0x00000060
.equ SIO_DIV_UDIVISOR_OFFSET,  0x00000064

.equ SIO_DIV_SDIVIDEND_OFFSET, 0x00000068
.equ SIO_DIV_SDIVISOR_OFFSET,  0x0000006c

.equ SIO_DIV_QUOTIENT_OFFSET,  0x00000070
.equ SIO_DIV_REMAINDER_OFFSET, 0x00000074

/**
 * Integer Divider
 *
 * Example of hardware divider signed and unsigned taken from the
 * Datasheet in chapter 2.3.1.5 (SIO)
 */

.macro __divider_delay
// delay 8 cycles
    b 1f
1:  b 1f
1:  b 1f
1:  b 1f
1:
.endm
.align 2

.global divmod_s32
divmod_s32:
    ldr r3, =(SIO_BASE)
    str r1, [r3, #SIO_DIV_SDIVIDEND_OFFSET]
    str r2, [r3, #SIO_DIV_SDIVISOR_OFFSET]
    __divider_delay

    // return 64 bit value so we can efficiently return 
    // both (note quotient must be read last)
    ldr r2, [r3, #SIO_DIV_REMAINDER_OFFSET]
    ldr r1, [r3, #SIO_DIV_QUOTIENT_OFFSET]
    str r1, [r0]
    //str r2, [r0, #32]
    bx lr

.global divmod_u32
divmod_u32:
    ldr r3, =(SIO_BASE)
    str r0, [r3, #SIO_DIV_UDIVIDEND_OFFSET]
    str r1, [r3, #SIO_DIV_UDIVISOR_OFFSET]
    __divider_delay
    // return 64 bit value so we can efficiently return 
    // both (note quotient must be read last)
    ldr r1, [r3, #SIO_DIV_REMAINDER_OFFSET]
    ldr r0, [r3, #SIO_DIV_QUOTIENT_OFFSET]
    bx lr
