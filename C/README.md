# pico c-sdk

## Installation

### Debian

Dependencies

```bash
sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi \
                 libstdc++-arm-none-eabi-newlib gdb-multiarch \
                 minicom
```

Openocd (with rp2040 support)

```bash
sudo apt remove openocd
cd /opt
git clone https://github.com/raspberrypi/openocd.git --recursive --branch rp2040 --depth=1
cd openocd
./bootstrap
./configure --enable-ftdi --enable-sysfsgpio --enable-bcm2835gpio
make -j10
sudo make install
```

Setup the environment 

```bash
cd /opt
git clone https://github.com/raspberrypi/pico-sdk.git
export PICO_SDK_PATH=/opt/pico-sdk # add this to .bashrc
# Copy pico_sdk_import.cmake from the SDK into your project directory
```

You may also add `-DPICO_SDK_PATH=/opt/pico-sdk` on the `cmake` command line.

[Example Pico Pi C](https://github.com/raspberrypi/pico-examples) code.

### vscode

Install the following extensions 

- marus25.cortex-debug
- ms-vscode.cmake-tools
- ms-vscode.cpptools

## Create a new Project

Easiest way is to use the `pico-project-generator`:

```bash
export PICO_SDK_PATH=/opt/pico-sdk # put this in your .bashrc or .profile 
../pico-project-generator/pico_project.py \
    --examples \
    --build \
    --project vscode \
    --usb \
    --debugger 1 \
    <ProjectName>
```

Debugger `1` is picoprobe (use another pico as hardware debugger with openocd).

Open project in vscode:

    PICO_SDK_PATH=/opt/pico-sdk code <ProjectName>

Select cmake Kit:

    GCC XX.X.X arm-non-eabi

```bash
$ ../pico-project-generator/pico_project.py -l
Available project features:

spi      SPI
i2c      I2C interface
dma      DMA support
pio      PIO interface
interp   HW interpolation
timer    HW timer
watch    HW watchdog
clocks   HW clocks
```

Minimal Project setup with openocd

```bash
export PICO_SDK_PATH=/opt/pico-sdk # put this in your .bashrc or .profile 
../pico-project-generator/pico_project.py \
    --build \
    --project vscode \
    --uart \
    --debugger 1 \
    <ProjectName>
```

## Serial console

### Linux

    minicom -b 115200 -D /dev/ttyACM0

OR

    screen /dev/ttyACM0 115200

## picoprobe

[Firmware Download (.uf2)](https://datasheets.raspberrypi.com/soft/picoprobe.uf2)

![picoprobe](../documentation/picoprobe.png)