#include <stdio.h>
#include "pico/stdlib.h"

// ASM functions
extern uint32_t max32(uint32_t v1, uint32_t v2);
extern void toggle_pin(uint32_t pin_number);
extern void init_pin_out(uint32_t pin_number);

/* this is required if we want to bl inside asm, quiet a curiosity :-/
void c_toggle_pin(int pin_number) {
    toggle_pin(pin_number);
}
*/

void loop() {
    while(true) {
        uint32_t v1 = 36;
        uint32_t v2 = 118;

        // use ASM function to calculate the max of two integers
        printf("max32(%d, %d): %d\n", v1, v2, max32(v1, v2));
        printf("max32(%d, %d): %d\n", v2, v1, max32(v1, v2));

        // fire our ASM function to toggle a pin
        toggle_pin(25);
        sleep_ms(200);
        toggle_pin(25);
        sleep_ms(200);

    }
}

int main() {
    stdio_init_all();

    /*
    gpio_init(PICO_DEFAULT_LED_PIN);
    gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
    gpio_pull_down(PICO_DEFAULT_LED_PIN);
    */
    init_pin_out(PICO_DEFAULT_LED_PIN);
    printf("aaaa");

    // blink once
    //gpio_put(PICO_DEFAULT_LED_PIN, 1);
    sleep_ms(250);
    gpio_put(PICO_DEFAULT_LED_PIN, 0);

    loop();

    return 0;
}
