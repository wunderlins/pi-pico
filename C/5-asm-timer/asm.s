.equ TIMELR,        0x4005400c
.equ ALARM1,        0x40054014
.equ ARMED,         0x40054020

//.equ GSIO_BASE,     0xd0000000
//.equ IO_BANK0,      0x40014000
//.equ TIMER_REG,     0x40054000
//.equ INTR,          0x40054034
//.equ INTE,          0x40054038

//.equ NVIC_ISER,     0xe000e100
//.equ VTOR,          0xe000ed08


/**
 * wait usecs
 * r0 32 bit int holding time
 * returns nothing
 *
 */
//void bwait_us(uint32_t microseconds);
.global bwait_us
bwait_us:
    // ALARM1 = TIMELR + microseconds;
    // while (bitRead(ARMED, 1));
    ldr r5, =TIMELR       // get current time
    ldr r4, [r5]
    add r4, r0            // this is our target time we set to ALARM
    ldr r5, =ALARM1       // pointer
    str r4, [r5]          // set ALARM1 to target time
    b   1f

    // check if we have reached the timeout
1:  nop                   // sleep 8 cycles
    nop
    nop
    nop
    nop
    nop
    nop
    nop

    ldr r4, =ARMED        // load timeout
    ldr r6, [r4]          // load timeout
    mov r4, #0b000000010  // check if armed bit 2 is high.
    and r6, r4
    cmp r6, #0b000000010
    bne 2f                // the bit is not high enymore, exit wait loop
    b   1b

    // return
2:  bx lr 

.equ GSIO_BASE,     0xd0000000
.equ IO_BANK0,      0x40014000

// extern void toggle_pin(uint32_t pin_number);
.global toggle_pin

// https://www.unshiu.com/posts/arm-assembly-pico-blink/
// this function expects a pin number in r0, it returns nothing
// The pin must be initialized for output already
toggle_pin:
  //push	{ r4, r5, r6, lr }
  ldr r4, =GSIO_BASE

  mov r5, #1              // set base pin
  lsl r5, r0              // shift into selected pin
  //mov r6, r5

  // Offset 20 (dec) is 0x14 (hex) which is the GPIO_OUT_SET register, 
  // so writing 1 « 25 into it sets GPIO 25 and turns on the LED.
  //str	r5, [r4, #0x14]         // turn pin on
  str	r5, [r4, #0x01c]         // xor pin with GPIO_OUT_XOR
  //str	r5, [r4, #24]       // turn off pin

  //pop { r4, r5, r6, pc }
  bx lr

.global init_pin_out

// extern void init_pin_out(uint32_t pin_number);
init_pin_out:

  //push	{ r4, r5, r6, lr }
  ldr r4, =GSIO_BASE

  mov r5, #1              // set base pin
  lsl r5, r0              // shift into selected pin

  // 36 (dec) is 0x24 (hex), and offset 0x24 is the GPIO_OE_SET register. 
  // OE is “output enable”, so writing 1 « 25 (r5) into it enables output to 
  // GPIO 25 (the LED on the Raspberry Pi Pico).
  str	r5, [r4, #0x4]      // disable input
  str	r5, [r4, #0x24]     // set pin as output


  // set pin FUNCTION to GIO
  ldr r4, =IO_BANK0
  mov r5, #8
  mul r5, r0 // GPIOn_CTRL offset is pin number * 8
  add r5, #4  // CTRL is foollowd one word after status
  mov r6, #5
  //add r4, r5 // addr of GPIO{r0}_CTRL
  str r6, [r4, r5] // set gpio function to SIO
  // set STATUS to 0
  mov r6, #0
  sub r4, #4
  str r6, [r4, r5] // set gpio function to SIO

  //pop { r4, r5, r6, pc }
  bx lr


/*
.global slp_cb
slp_cb:
    // clear the interrupt in INTR:
    ldr r4, =INTR
    mov r5, #2   // same as INTE (bit 2)
    str	r5, [r4]

    // debug: turn led on 
    b led_on

    bx lr // rturn


.global slp_ms
slp_ms:

    // 1. convert input parameter
    //
    // input is in ms, timer requires us, multiply by 1000
    mov r4, #125
    lsl r4, #3     // 1000
    mul r0, r4     // convert input from us to ms

    // 2. stup exeption/interupt handler
    // 
    // define out intrupt handler (which is an exception handler in arm speak).
    // first custom handler is at offset 16
    // VTOR + (16 + TIMER_IRQ_1) * 4
    mov r4, #16
    add r4, #1   // TIMER_IRQ_1
    mov r5, #4
    mul r4, r5   // convert offset from bytes to words
    ldr r5, =VTOR
    add r4, r5   // this is the final address of the interupt.
    // set address of callback function in r4
    ldr r5, =slp_cb
    str	r5, [r4]

    // 2. Enable the interrupt in the Timer (INTE) register
    ldr r4, =INTE
    mov r5, #2   // ALARM_1 is at bit 2
    str	r5, [r4]

    // 3. Enable the NVIC interrupt (NVIC_ISER)
    ldr r4, =NVIC_ISER
    mov r5, #2   // same as TIMER_IRQ_1 (bit 2)
    str	r5, [r4]

    // 4. Set the ALARM1 register to the target time
    //ALARM1 = TIMELR + 250000;
    ldr r4, =ALARM1
    ldr r5, =TIMELR
    ldr r6, [r5] // load current low time
    add r6, r0   // add timeout
    str	r6, [r4]

    bx lr
*/