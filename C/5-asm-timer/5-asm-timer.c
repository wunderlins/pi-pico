#include <stdio.h>
#include "pico/stdlib.h"
// functions implemented in assembly
#include "asm.h"

#define LED_PIN PICO_DEFAULT_LED_PIN
static int state = 0;

/*
void led_toggle() {
    puts("Hello, callback!");
    state = 1 - state;
    gpio_put(LED_PIN, state);
}
*/

volatile uint32_t *vtr;

int main() {

    stdio_init_all();

    // GPIO initialisation.
    // We will make this GPIO an output
    /*
    gpio_init(LED_PIN);
    gpio_pull_up(LED_PIN); // pin should go high
    */

    // this is the asm equivalenet of the above 2 lines
    init_pin_out(LED_PIN);

    puts("Hello, world!");

    while(1) {
        /* toggle pin in C
        state = 1 - state;
        gpio_put(LED_PIN, state);
        */
        toggle_pin(LED_PIN);

        //sleep_ms(1000);
        bwait_us(1e6); // 1 second
    }

    return 0;
}
