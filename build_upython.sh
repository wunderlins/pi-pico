#!/usr/bin/env bash

sudo apt install -y cmake gcc-arm-none-eabi \
                    libnewlib-arm-none-eabi build-essential

cd micropython
git submodule update --init -- lib/pico-sdk lib/tinyusb

# First we need to bootstrap a special tool for MicroPython 
# builds, that ships with the source code:
make -C mpy-cross

# We can now build the port we need for RP2040, that is, the 
# version of MicroPython that has specific support for our
# chip.

cd ports/rp2
make clean

# enable uard REPL
#define MICROPY_HW_ENABLE_UART_REPL  (1)
sed -i -e 's/MICROPY_HW_ENABLE_UART_REPL.*/MICROPY_HW_ENABLE_UART_REPL (1)/' mpconfigport.h 
# disable uard REPL
#define MICROPY_HW_ENABLE_UART_REPL  (0)
#sed -i -e 's/MICROPY_HW_ENABLE_UART_REPL.*/MICROPY_HW_ENABLE_UART_REPL (0)/' mpconfigport.h 

if [[ ! -f build-PICO/firmware.uf2 ]]; then
    make -j10
fi

cd ../../../
echo "==> Binary at: ./micropython/ports/rp2/build-PICO/firmware.uf2"
